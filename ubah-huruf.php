<?php
function ubah_huruf($string){
//kode di sini
    for($i = 0; $i < strlen($string); $i++) {
        $string_baru[$i] = chr(ord($string[$i]) +1); //mengubah ke karakter baru dari nilai pertama string
    }
    return implode($string_baru). "<br>"; // mengembalikan nilai string dari elemen array
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>