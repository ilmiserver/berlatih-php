<?php
function tentukan_nilai($number)
{
    if ($number >=85 && $number <=100) {
        return "Sangat Baik";
    }else if($number >70 && $number <=85) {
        return "Baik";
    }else if ($number >=60 && $number < 70) {
        return "Cukup";
    }else {
        return "Kurang";
    }
}

//TEST CASES
echo "Nilai(85-100) : " . tentukan_nilai(98) . "<br>"; //Sangat Baik
echo "Nilai(70-85): " . tentukan_nilai(76) . "<br>"; //Baik
echo "Nilai(60-70): " . tentukan_nilai(67) . "<br>"; //Cukup
echo "Nilai(0-60): " . tentukan_nilai(43); //Kurang
?>